package com.example.movie_characters_api.repository;

import com.example.movie_characters_api.model.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CharacterRepository extends JpaRepository<MovieCharacter, Integer> {
}
