package com.example.movie_characters_api.mapper;


import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.model.dto.MovieCharacterDTO;
import com.example.movie_characters_api.model.dto.MovieDTO;
import com.example.movie_characters_api.service.FranchiseService;
import com.example.movie_characters_api.service.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract MovieCharacterDTO characterToCharacterDto(MovieCharacter character);

    // Mapping the other way
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract MovieCharacter characterDtoToCharacter(MovieCharacterDTO characterDTO);

    //Mapping collection, implemented automatically
    public abstract Collection<MovieCharacterDTO> characterToCharacterDto(Collection<MovieCharacter> characters);

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(movie -> movie.getId()).collect(Collectors.toSet());
    }

    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> movieIds) {
        if (movieIds == null) {
            return new HashSet<>();
        }
        return movieIds.stream()
                .map( id -> movieService.findById(id))
                .collect(Collectors.toSet());
    }
}
