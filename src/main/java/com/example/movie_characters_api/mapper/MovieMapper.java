package com.example.movie_characters_api.mapper;

import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.model.dto.MovieDTO;
import com.example.movie_characters_api.service.CharacterService;
import com.example.movie_characters_api.service.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    protected FranchiseService franchiseService;

    @Autowired
    protected CharacterService characterService;

    @Mapping(target = "franchise", source = "franchise.id")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "charactersToIds")
    public abstract MovieDTO movieToMovieDto(Movie movie);

    // Mapping the other way
    @Mapping(target = "franchise", source = "franchise", qualifiedByName = "franchiseIdToFranchise")
    @Mapping(target = "characters", source = "characters", qualifiedByName = "characterIdsToCharacters")
    public abstract Movie movieDtoToMovie(MovieDTO movieDTO);

    //Mapping collection, implemented automatically
    public abstract Collection<MovieDTO> movieToMovieDto(Collection<Movie> movies);

    @Named("franchiseIdToFranchise")
    Franchise mapIdToFranchise(Integer id) {
        if(id != null) {
            return franchiseService.findById(id);
        }
        return null;
    }

    @Named("charactersToIds")
    Set<Integer> mapCharactersToIds(Set<MovieCharacter> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(character -> character.getId()).collect(Collectors.toSet());
    }

    @Named("characterIdsToCharacters")
    Set<MovieCharacter> mapIdsToCharacters(Set<Integer> characterIds) {
        if (characterIds == null) {
            return new HashSet<>();
        }
        return characterIds.stream()
                .map( id -> characterService.findById(id))
                .collect(Collectors.toSet());
    }

}
