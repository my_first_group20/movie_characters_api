package com.example.movie_characters_api.mapper;

import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.dto.FranchiseDTO;
import com.example.movie_characters_api.service.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {

    @Autowired
    protected MovieService movieService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    public abstract FranchiseDTO franchiseToFranchiseDto(Franchise franchise);

    // Mapping the other way
    @Mapping(target = "movies", source = "movies", qualifiedByName = "movieIdsToMovies")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDTO franchiseDTO);

    //Mapping collection, implemented automatically
    public abstract Collection<FranchiseDTO> franchiseToFranchiseDto(Collection<Franchise> franchises);

    @Named("moviesToIds")
    Set<Integer> mapMoviesToIds(Set<Movie> source) {
        if(source == null)
            return null;
        return source.stream()
                .map(movie -> movie.getId()).collect(Collectors.toSet());
    }

    @Named("movieIdsToMovies")
    Set<Movie> mapIdsToMovies(Set<Integer> movieIds) {
        if (movieIds == null) {
            return new HashSet<>();
        }
        return movieIds.stream()
                .map( id -> movieService.findById(id))
                .collect(Collectors.toSet());
    }
}
