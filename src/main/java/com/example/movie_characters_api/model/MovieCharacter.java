package com.example.movie_characters_api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;


@Entity
@Getter
@Setter
@Table(name = "character")
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "char_id")
    Integer id;

    @Column(name = "char_name")
    String name;
    String alias;
    String gender;
    String picture;

    @ManyToMany(mappedBy = "characters")
    private Set<Movie> movies = new HashSet<>();
}
