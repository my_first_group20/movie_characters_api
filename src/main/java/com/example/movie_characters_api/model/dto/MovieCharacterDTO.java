package com.example.movie_characters_api.model.dto;

import lombok.Data;

import javax.persistence.Column;
import java.util.Set;

@Data
public class MovieCharacterDTO {
    Integer id;
    String name;
    String alias;
    String gender;
    String picture;
    Set<Integer> movies;
}
