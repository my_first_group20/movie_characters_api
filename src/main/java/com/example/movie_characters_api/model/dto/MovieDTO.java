package com.example.movie_characters_api.model.dto;

import com.example.movie_characters_api.model.MovieCharacter;
import lombok.Data;

import javax.persistence.Column;
import java.util.Set;

@Data
public class MovieDTO {
    Integer id;
    String title;
    String genre;
    Integer releaseYear;
    String director;
    String picture;
    String trailer;
    Integer franchise;
    Set<Integer> characters;
}
