package com.example.movie_characters_api.model.dto;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDTO {
    Integer id;
    String name;
    String description;
    Set<Integer> movies;
}
