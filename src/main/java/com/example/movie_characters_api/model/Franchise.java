package com.example.movie_characters_api.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
public class Franchise {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    Integer id;

    @Column(name = "franchise_name", length = 100, nullable = false)
    String name;

    @Column(length = 500)
    String description;

    @OneToMany(mappedBy = "franchise")
    private Set<Movie> movies;
}
