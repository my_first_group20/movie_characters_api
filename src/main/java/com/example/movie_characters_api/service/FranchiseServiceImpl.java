package com.example.movie_characters_api.service;

import com.example.movie_characters_api.exception.DocumentNotFoundException;
import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.repository.FranchiseRepository;
import com.example.movie_characters_api.repository.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;
    private final MovieRepository movieRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).orElseThrow(() -> new DocumentNotFoundException(id));
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise toSave) {
        return franchiseRepository.save(toSave);
    }

    //if body lacks any property, will be set to null
    @Override
    public Franchise update(Franchise updateFranchise) {
        Franchise franchiseFromDB = franchiseRepository.findById(updateFranchise.getId()).get();
        franchiseFromDB.setName(updateFranchise.getName());
        franchiseFromDB.setDescription(updateFranchise.getDescription());
        Franchise updatedFranchise = franchiseRepository.save(franchiseFromDB);
        return updatedFranchise;
    }

    @Override
    public void deleteById(Integer id) {
        Franchise franchiseFromDB = franchiseRepository.findById(id).get();
        franchiseFromDB.getMovies().forEach(movie -> movie.setFranchise(null));
        franchiseRepository.deleteById(id);
    }

    @Override
    public void delete(Franchise entity) {

    }

    @Override
    public Collection<Movie> findAllMoviesInFranchise(Integer id) {
        return franchiseRepository.findById(id).get().getMovies();
    }

    //todo: jó így? update: jónak tűnik, edge case-ek nélkül
    @Override
    public Collection<MovieCharacter> findAllCharactersInFranchise(Integer id) {
        Collection<Movie> moviesInFranchise = franchiseRepository.findById(id).get().getMovies();
        moviesInFranchise.forEach(movie -> movieRepository.findById(movie.getId()).get().getCharacters());
        Set<MovieCharacter> charactersInFranchise = new HashSet<>();
        moviesInFranchise.forEach(movie -> {
            movieRepository.findById(movie.getId()).get().getCharacters().forEach( character -> charactersInFranchise.add(character));
        });
        return charactersInFranchise;
    }

    @Override
    public void updateAllMoviesInFranchise(Set<Integer> movieIDs, Integer id) {
        Franchise franchiseFromDB = franchiseRepository.findById(id).get();
        franchiseFromDB.getMovies().forEach(movie -> movie.setFranchise(null));
        movieIDs.forEach(movieID -> movieRepository.findById(movieID).get().setFranchise(franchiseFromDB));
    }
}
