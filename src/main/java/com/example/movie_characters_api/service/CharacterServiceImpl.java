package com.example.movie_characters_api.service;

import com.example.movie_characters_api.exception.DocumentNotFoundException;
import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.repository.CharacterRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

@Service
@Transactional
public class CharacterServiceImpl implements CharacterService{
    private final CharacterRepository characterRepository;
    private final Logger logger = LoggerFactory.getLogger(CharacterServiceImpl.class);

    public CharacterServiceImpl(CharacterRepository characterRepository) {
        this.characterRepository = characterRepository;
    }

    @Override
    public MovieCharacter findById(Integer id) {
        return characterRepository.findById(id).orElseThrow(() -> new DocumentNotFoundException(id));
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return characterRepository.findAll();
    }

    @Override
    public MovieCharacter add(MovieCharacter toSave) {
        return characterRepository.save(toSave);
    }

    //if body lacks any property, will be set to null
    @Override
    public MovieCharacter update(MovieCharacter updateMovieCharacter) {
        MovieCharacter characterFromDB = characterRepository.findById(updateMovieCharacter.getId()).get();
        characterFromDB.setName(updateMovieCharacter.getName());
        characterFromDB.setAlias(updateMovieCharacter.getAlias());
        characterFromDB.setGender(updateMovieCharacter.getGender());
        characterFromDB.setPicture(updateMovieCharacter.getPicture());
        MovieCharacter updatedMovieCharacter = characterRepository.save(characterFromDB);
        return updatedMovieCharacter;
    }

    @Override
    public void deleteById(Integer id) {
        MovieCharacter characterFromDB = characterRepository.findById(id).get();
        characterFromDB.getMovies().forEach(movie -> movie.getCharacters().remove(characterFromDB));
        characterRepository.deleteById(id);
    }

    @Override
    public void delete(MovieCharacter entity) {

    }
}
