package com.example.movie_characters_api.service;

import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;

import java.util.Collection;
import java.util.Set;

public interface MovieService extends CRUDService<Movie, Integer> {
    public Collection<MovieCharacter> findAllCharactersInMovie(Integer id);

    public void updateAllCharactersInMovie(Set<Integer> characters, Integer id);
}
