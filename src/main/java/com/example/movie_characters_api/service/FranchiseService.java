package com.example.movie_characters_api.service;

import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;

import java.util.Collection;
import java.util.Set;

public interface FranchiseService extends CRUDService<Franchise, Integer> {
    public Collection<Movie> findAllMoviesInFranchise(Integer id);

    public Collection<MovieCharacter> findAllCharactersInFranchise(Integer id);

    public void updateAllMoviesInFranchise(Set<Integer> movies, Integer id);
}
