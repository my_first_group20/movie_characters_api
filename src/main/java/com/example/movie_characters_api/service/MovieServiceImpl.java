package com.example.movie_characters_api.service;

import com.example.movie_characters_api.exception.DocumentNotFoundException;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.repository.CharacterRepository;
import com.example.movie_characters_api.repository.MovieRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;
    private final CharacterRepository characterRepository;
    private final Logger logger = LoggerFactory.getLogger(MovieServiceImpl.class);

    @Autowired
    private WebSocketService webSocketService;

    public MovieServiceImpl(MovieRepository movieRepository, CharacterRepository characterRepository) {
        this.movieRepository = movieRepository;
        this.characterRepository = characterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).orElseThrow(() -> new DocumentNotFoundException(id));
    }

    @Override
    public Collection<Movie> findAll() {
        return movieRepository.findAll();
    }

    @Override
    public Movie add(Movie toSave) {
        // Notify frontend that there has been a change on entity
        notifyFrontend();

        return movieRepository.save(toSave);
    }

    //if body lacks any property, will be set to null
//    @Override
//    public Movie update(Movie updateMovie) {
//        Movie movieFromDB = movieRepository.findById(updateMovie.getId()).get();
//        movieFromDB.setTitle(updateMovie.getTitle());
//        movieFromDB.setGenre(updateMovie.getGenre());
//        movieFromDB.setReleaseYear(updateMovie.getReleaseYear());
//        movieFromDB.setDirector(updateMovie.getDirector());
//        movieFromDB.setPicture(updateMovie.getPicture());
//        movieFromDB.setTrailer(updateMovie.getTrailer());
//        movieFromDB.setFranchise(updateMovie.getFranchise());
//        Movie updatedMovie = movieRepository.save(movieFromDB);
//        return updatedMovie;
//    }
    @Override
    public Movie update(Movie updateMovie) {

        // Notify frontend that there has been a change on entity
        notifyFrontend();
        return movieRepository.save(updateMovie);
    }

    @Override
    public void deleteById(Integer id) {
        movieRepository.deleteById(id);
        // Notify frontend that there has been a change on entity
        notifyFrontend();
    }

    @Override
    public void delete(Movie entity) {

    }

    @Override
    public Collection<MovieCharacter> findAllCharactersInMovie(Integer id) {
        return movieRepository.findById(id).get().getCharacters();
    }

    @Override
    public void updateAllCharactersInMovie(Set<Integer> characterIDs, Integer id) {
        Movie movieFromDB = movieRepository.findById(id).get();

        //empty movie's character set
        movieFromDB.setCharacters(new HashSet<>());

        //loop through chars from parameter, add each character to movie's character set
        characterIDs.forEach(characterID -> {
            MovieCharacter charFromDB = characterRepository.findById(characterID).get();
            movieFromDB.getCharacters().add(charFromDB);
        });
    }


    public void notifyFrontend() {
        webSocketService.sendMessage("movie");
    }
}
