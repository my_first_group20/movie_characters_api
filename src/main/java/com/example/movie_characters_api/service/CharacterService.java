package com.example.movie_characters_api.service;


import com.example.movie_characters_api.model.MovieCharacter;

public interface CharacterService extends CRUDService<MovieCharacter, Integer> {
}
