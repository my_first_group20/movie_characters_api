package com.example.movie_characters_api.controller;

import com.example.movie_characters_api.mapper.CharacterMapper;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.MovieCharacter;
import com.example.movie_characters_api.model.dto.MovieCharacterDTO;
import com.example.movie_characters_api.model.dto.MovieDTO;
import com.example.movie_characters_api.service.CharacterService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

@RestController
@RequestMapping(path = "api/characters")
public class CharacterController {

    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }
        @GetMapping
        public ResponseEntity<Collection<MovieCharacterDTO>> findAll() {
            Collection<MovieCharacterDTO> characterDTOs = characterMapper.characterToCharacterDto(characterService.findAll());
            return ResponseEntity.ok(characterDTOs);
        }

        @GetMapping("{id}")
        public ResponseEntity<MovieCharacterDTO> findById(@PathVariable int id) {
            MovieCharacterDTO characterDTO = characterMapper.characterToCharacterDto(characterService.findById(id));
            return ResponseEntity.ok(characterDTO);
        }

        @PostMapping
        public ResponseEntity<MovieCharacterDTO> add(@RequestBody MovieCharacter characterToSave) {
            MovieCharacterDTO characterSavedDTO = characterMapper.characterToCharacterDto(characterService.add(characterToSave));
            return new ResponseEntity<>(characterSavedDTO, HttpStatus.CREATED);
        }

        //replaces whole obj with the one received in the body!
        //missing fields will be set tu null
        @PutMapping("{id}")
        public ResponseEntity<MovieCharacterDTO> update(@RequestBody MovieCharacterDTO updateData, @PathVariable int id) {
            // Validates if body is correct
            if(id != updateData.getId())
                return ResponseEntity.badRequest().build();
            MovieCharacter updateCharacter = characterMapper.characterDtoToCharacter(updateData);
            MovieCharacterDTO updatedCharacter = characterMapper.characterToCharacterDto(characterService.update(updateCharacter));
            return new ResponseEntity<>(updatedCharacter, HttpStatus.OK);
        }

        @DeleteMapping("{id}")
        public ResponseEntity delete(@PathVariable int id) {
            characterService.deleteById(id);
            return ResponseEntity.noContent().build();
        }

}
