package com.example.movie_characters_api.controller;

import com.example.movie_characters_api.mapper.CharacterMapper;
import com.example.movie_characters_api.mapper.FranchiseMapper;
import com.example.movie_characters_api.mapper.MovieMapper;
import com.example.movie_characters_api.model.Franchise;
import com.example.movie_characters_api.model.dto.FranchiseDTO;
import com.example.movie_characters_api.model.dto.MovieCharacterDTO;
import com.example.movie_characters_api.model.dto.MovieDTO;
import com.example.movie_characters_api.service.CharacterService;
import com.example.movie_characters_api.service.FranchiseService;
import com.example.movie_characters_api.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@RequestMapping(path = "api/franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;
    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final MovieService movieService;
    private final CharacterMapper characterMapper;
    private final CharacterService characterService;


    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, MovieService movieService, CharacterMapper characterMapper, CharacterService characterService) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.movieService = movieService;
        this.characterMapper = characterMapper;
        this.characterService = characterService;
    }


    @GetMapping
    public ResponseEntity<Collection<FranchiseDTO>> findAll() {
        Collection<FranchiseDTO> franchiseDTOs = franchiseMapper.franchiseToFranchiseDto(franchiseService.findAll());
        return ResponseEntity.ok(franchiseDTOs);
    }

    @GetMapping("{id}")
    public ResponseEntity<FranchiseDTO> findById(@PathVariable int id) {
        FranchiseDTO franchiseDTO = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDTO);
    }

    @PostMapping
    public ResponseEntity<FranchiseDTO> add(@RequestBody Franchise franchiseToSave) {
        FranchiseDTO franchiseSavedDTO = franchiseMapper.franchiseToFranchiseDto(franchiseService.add(franchiseToSave));
        return new ResponseEntity<>(franchiseSavedDTO, HttpStatus.CREATED);
    }

    @PutMapping("{id}")
    public ResponseEntity<FranchiseDTO> update(@RequestBody FranchiseDTO updateData, @PathVariable int id) {
        // Validates if body is correct
        if(id != updateData.getId())
            return ResponseEntity.badRequest().build();
        Franchise updateFranchise = franchiseMapper.franchiseDtoToFranchise(updateData);
        FranchiseDTO updatedFranchise = franchiseMapper.franchiseToFranchiseDto(franchiseService.update(updateFranchise));
        return new ResponseEntity<>(updatedFranchise, HttpStatus.OK);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/movies")
    public ResponseEntity<Collection<MovieDTO>> findAllMoviesInFranchise(@PathVariable int id) {
        Collection<MovieDTO> movieDTOs = movieMapper.movieToMovieDto(franchiseService.findAllMoviesInFranchise(id));
        return ResponseEntity.ok(movieDTOs);
    }

    @PutMapping("{id}/movies")
    public ResponseEntity update(@RequestBody Integer[] updateData, @PathVariable int id) {
        //array to Set of IDs
        Set<Integer> updateMovieIDs = new HashSet<>(Arrays.asList(updateData));
        franchiseService.updateAllMoviesInFranchise(updateMovieIDs, id);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<MovieCharacterDTO>> findAllCharactersInFranchise(@PathVariable int id) {
        Collection<MovieCharacterDTO> characterDTOS = characterMapper.characterToCharacterDto(franchiseService.findAllCharactersInFranchise(id));
        return ResponseEntity.ok(characterDTOS);
    }
}
