package com.example.movie_characters_api.controller;

import com.example.movie_characters_api.mapper.CharacterMapper;
import com.example.movie_characters_api.mapper.MovieMapper;
import com.example.movie_characters_api.model.Movie;
import com.example.movie_characters_api.model.dto.MovieCharacterDTO;
import com.example.movie_characters_api.model.dto.MovieDTO;
import com.example.movie_characters_api.service.MovieService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

@RestController
@CrossOrigin
@RequestMapping(path = "api/movies")
public class MovieController {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @CrossOrigin
    @GetMapping
    public ResponseEntity<Collection<MovieDTO>> findAll() {
        Collection<MovieDTO> movieDTOs = movieMapper.movieToMovieDto(movieService.findAll());
        return ResponseEntity.ok(movieDTOs);
    }

    @CrossOrigin
    @GetMapping("{id}")
    public ResponseEntity<MovieDTO> findById(@PathVariable int id) {
        MovieDTO movieDTO = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDTO);
    }

    @CrossOrigin
    @PostMapping
    public ResponseEntity<MovieDTO> add(@RequestBody Movie movieToSave) {
        System.out.println(movieToSave.getId());
        MovieDTO movieSavedDTO = movieMapper.movieToMovieDto(movieService.add(movieToSave));
        return new ResponseEntity<>(movieSavedDTO, HttpStatus.CREATED);
    }

//    //todo: megnézni, miért nem jó, hibaüzi: The given id must not be null!;
//    //már print sorban dobja
//    @PostMapping
//    public ResponseEntity<MovieDTO> add(@RequestBody MovieDTO createData) {
//        Movie createMovie = movieMapper.movieDtoToMovie(createData);
//        System.out.println(createMovie.getId());
////        MovieDTO movieSavedDTO = movieMapper.movieToMovieDto(movieService.add(createMovie));
////        return new ResponseEntity<>(movieSavedDTO, HttpStatus.CREATED);
//        return null;
//    }

    //replaces whole obj with the one received in the body!
    //missing fields will be set tu null
    @CrossOrigin
    @PutMapping("{id}")
    public ResponseEntity<MovieDTO> update(@RequestBody MovieDTO updateData, @PathVariable int id) {
        // Validates if body is correct
        if(id != updateData.getId())
            return ResponseEntity.badRequest().build();
        Movie updateMovie = movieMapper.movieDtoToMovie(updateData);
        MovieDTO updatedMovie = movieMapper.movieToMovieDto(movieService.update(updateMovie));
        return new ResponseEntity<>(updatedMovie, HttpStatus.OK);
    }

    @CrossOrigin
    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable int id) {
        movieService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @CrossOrigin
    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<MovieCharacterDTO>> findAllCharactersInFranchise(@PathVariable int id) {
        Collection<MovieCharacterDTO> characterDTOs = characterMapper.characterToCharacterDto(movieService.findAllCharactersInMovie(id));
        return ResponseEntity.ok(characterDTOs);
    }

    @CrossOrigin
    @PutMapping("{id}/characters")
    public ResponseEntity updateAllCharactersInMovie(@RequestBody Integer[] updateData, @PathVariable int id) {
        //array to Set of IDs
        Set<Integer> updateCharacterIDs = new HashSet<>(Arrays.asList(updateData));
        movieService.updateAllCharactersInMovie(updateCharacterIDs, id);
        return ResponseEntity.noContent().build();
    }
}
