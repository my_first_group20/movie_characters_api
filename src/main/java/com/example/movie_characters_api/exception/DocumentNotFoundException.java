package com.example.movie_characters_api.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class DocumentNotFoundException extends RuntimeException{
    public DocumentNotFoundException(int id) {
        super("No document found with ID " + id);
    }
}
