import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Movie} from "../models/movie";

@Injectable({
  providedIn: 'root'
})
export class MovieService {

  private readonly MOVIE_URL: string = "http://localhost:8080/api/movies";

  constructor(private http: HttpClient) { }

  public getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>(this.MOVIE_URL);
  }

  public getMovie(movieId: string): Observable<Movie> {
    return this.http.get<Movie>(`${this.MOVIE_URL}/${movieId}`);
  }

  public saveMovie(movie: Movie): Observable<Movie> {
    return this.http.post<Movie>(this.MOVIE_URL, movie)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public updateMovie(movie: Movie): Observable<Movie> {
    return this.http.put<Movie>(`${this.MOVIE_URL}/${movie.id}`, movie)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public patchMovie(movie: Movie, body: any): Observable<Movie> {
    return this.http.put<Movie>(`${this.MOVIE_URL}/${movie.id}`, body)
    // todo backenről: Az elmentett objektumot adja vissza?
  }

  public deleteMovie(movieId: string): Observable<void> {
    return this.http.delete<void>(`${this.MOVIE_URL}/${movieId}`)
    // todo backenről: Üres objektumot ad vissza
  }
}
