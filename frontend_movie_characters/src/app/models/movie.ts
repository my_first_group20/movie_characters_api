export interface Movie {
  id?: number,
  title: string,
  genre?: string,
  releaseYear?: number,
  director?: string,
  picture?: string,
  trailer?: string,
  franchise?: number,
  characters?: number[]
}


