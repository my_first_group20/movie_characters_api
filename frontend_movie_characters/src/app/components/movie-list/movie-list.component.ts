import { Component, OnInit } from '@angular/core';
import {Movie} from "../../models/movie";
import {MovieService} from "../../services/movie.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {StompService} from "../../services/stomp.service";

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html',
  styleUrls: ['./movie-list.component.scss']
})
export class MovieListComponent implements OnInit {

  public movies: Movie[] = [];
  public addMovieForm: FormGroup = new FormGroup({
    title: new FormControl("", Validators.required)
  })

  constructor(private movieService: MovieService,
              private router: Router,
              private stompService: StompService) { }

  ngOnInit(): void {
    //load list once when component loads
    this.movieService.getMovies().subscribe({
      next: (moviesFromServer: Movie[]) => {
        this.movies = moviesFromServer;
        console.log(this.movies);
      },
      error: (e) => console.log(e)
    })

    //subscribe to websocket notification, and load list every time a new movie is saved on any connected client
    this.stompService.subscribe("/topic/movie", ():void => {    //topic: what you declared in backend WebSocketConfiguration as
                                                                          //messageBroker prefix
                                                                       //movie: what you declared in backend movie service as topic suffix
      console.log("notified")
      this.movieService.getMovies().subscribe({
        next: (moviesFromServer: Movie[]) => {
          this.movies = moviesFromServer;
          console.log(this.movies);
        },
        error: (e) => console.log(e)
      })
    })

  }

  onSubmit() {
    if (this.addMovieForm.valid) {
      console.log(this.addMovieForm.get("title")?.value)
      const newMovie: Movie = {
        title: this.addMovieForm.get("title")?.value
      }
      this.movieService.saveMovie(newMovie).subscribe({
        next: () => {
          this.addMovieForm.reset()
          console.log("movie added")
        },
        error: (e) => {
          console.log(e)
        }
      })
    }
  }
}
