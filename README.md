# API and Client to demonstrate websocket communication

**Full stack project** with **SpringBoot, Angular** and **websocket**

## Usage

- clone repo
- make database in PgAdmin called moviecharacters, configure connection in IntelliJ
- start Java app
- enter frontend directory:```cd frontend_movie_characters```
- run ```npm i```
- start Angular app with ```ng s```
- open two browsers, navigate to http://localhost:4200
- add new movie in one browser: it appears in the movie list of both browsers
- note: server uses websocket connection to notify all connected clients of
  the change. Clients react to the notification by sending a new http request to
get the refreshed list of movies

